$(function () {
    var size = getClientSize();
    $(".view-video").css('height',size.height);
    $(".fullSize").css('width',size.width);
})

window.onresize = function() {
    var size = getClientSize();
    /*alert("Width: " + size.width + ", Height: " + size.height);*/
    /*$(".fullSize").css({width: size.width,height: size.height});*/
    $(".view-video").css('height',size.height);
    $(".fullSize").css('width',size.width);
}

function getClientSize() {
    var width = 0, height = 0;

    if(typeof(window.innerWidth) == 'number') {
        width = window.innerWidth;
        height = window.innerHeight;
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        width = document.documentElement.clientWidth;
        height = document.documentElement.clientHeight;
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        width = document.body.clientWidth;
        height = document.body.clientHeight;
    }

    return {width: width, height: height};
}